---
title: Hijacket Vendulum Cora Blue - HJ-VD
description: Jual jaket muslimah Hijacket Vendulum Cora Blue - HJ-VD
date: '2018-07-15T17:48:14+07:00'
slug: hj-vd-cora-blue
product:
  - vendulum
brand:
  - hijacket
thumbnail: /images/vendulum/vd-cora-blue.jpg
image:
  - /images/vendulum/vd-cora-blue-1.jpg
  - /images/vendulum/vd-cora-blue-2.jpg
  - /images/vendulum/vd-cora-blue-3.jpg
  - /images/vendulum/vd-cora-blue-4.jpg
  - /images/vendulum/vd-cora-blue-5.jpg
sku: HJ-VD-CORA-BLUE
badge: ''
berat: 700 gram
color:
  - Cora Blue
size:
  - name: All Size
    price: 210000
  - name: XL
    price: 220000
stock: true
---

Hijacket Vendulum Original

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : Premium Fleece yang "SOFT TOUCH" langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Sablonan Berkualitas

- ▶️ Bukan sekedar fashion. Namun menguatkan "JATI DIRI / IDENTITAS" Hijaber yang modis dan stylish

Ada 4 Warna Favorit

#### Tabel Ukuran Hijacket Vendulum Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 89-90           | 89-90  	      |
