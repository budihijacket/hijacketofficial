---
title: Hijacket Japan Street HJ-JS4
description: Jual jaket muslimah Hijacket Japan Street HJ-JS4
date: '2018-04-04T17:48:14+07:00'
slug: hj-js4
product:
  - japan street
brand:
  - hijacket
thumbnail: /images/japanstreet/js4.jpg
image:
  - /images/japanstreet/js4-1.jpg
  - /images/japanstreet/js4-2.jpg
  - /images/japanstreet/js4-3.jpg
  - /images/japanstreet/js4-4.jpg
  - /images/japanstreet/js4-5.jpg
  - /images/japanstreet/js4-6.jpg
sku: HJ-JS4
badge: ''
berat: 700 gram
color:
  - Black
size:
  - name: All Size
    price: 185000
  - name: XL
    price: 195000
stock: true
---

HIJACKET JAPAN STREET ORIGINAL terinspirasi dari negeri sakura. Buat kamu yang suka Jepang, Hijacket Japan Street dengan sablon 旅行者(Traveler) siap menemani perjalanan & gaya kamu ke negeri sakura.

- ▶️ Ukuran : ALL SIZE FIT TO L dan XL (XL Nambah 10.000)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Sablonan Berkualitas

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Japan Street Original

#### Tabel Ukuran Hijacket Japan Street Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 93-95           | 93-95  	      |
