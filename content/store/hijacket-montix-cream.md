---
title: Hijacket Montix Cream - HJ-MT
description: Jual jaket muslimah Hijacket Montix Cream - HJ-MT
date: '2018-04-04T17:48:14+07:00'
slug: hj-mt-cream
product:
  - montix
brand:
  - hijacket
thumbnail: /images/montix/montix-cream.jpg
image:
  - /images/montix/montix-cream-1.jpg
  - /images/montix/montix-cream-2.jpg
  - /images/montix/montix-cream-3.jpg
  - /images/montix/montix-cream-4.jpg
  - /images/montix/montix-cream-5.jpg
sku: HJ-MT-CREAM
badge: ''
berat: 700 gram
color:
  - Cream
size:
  - name: All Size
    price: 235000
  - name: XL
    price: 245000
stock: true
---

HIJACKET MONTIX ORIGINAL, adventuring-inspired layer for Hijaber with a color-blocked design on lightweight fabric for an iconic look 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron, Pocket Inside.

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Sablonan Berkualitas + Pocket Inside

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 7 variasi warna Hijacket Montix Original

#### Tabel Ukuran Hijacket Montix Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 104-106         | 110-112	      |
| Lingkar Lengan  | 48-50           | 52-54  	      |
| Panjang Tangan  | 58-60           | 58-60  	      |
| Panjang Badan   | 83-85           | 85-87  	      |
