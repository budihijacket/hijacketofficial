---
title: Konfirmasi Pembayaran
description: Konfirmasi Pembayaran
date: '2018-03-30T10:34:07+07:00'
---
Untuk memudahkan kami dalam mengecek pembayaran Anda, silahkan konfirmasi pembayaran dengan menghubungi Customer Service kami setelah melakukan transfer/setoran ke rekening kami disertai dengan bukti transfer.

**BCA No. 5800183521  a/n: Syamsudin Budi Nugroho**

**MANDIRI No. 070-00030-46922 a/n: Syamsudin Budi Nugroho**

Kami akan segera melakukan pengecekan atas transfer Anda dan memproses pengiriman barang Anda.

Terima Kasih.
