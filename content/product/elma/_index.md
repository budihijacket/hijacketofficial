---
title: Hijacket Elma Original
description: "Hijacket Elma Original merupakan seri hijacket gaya modern yang tak lekang oleh waktu, hijacket elma dilengkapi dengan bentuk feminin, saku yang praktis dan pas serbaguna dan dengan tombol pearlescent vintage-chic." 
image: "/images/hijacket-elma-cover.png"
slug: "elma"
---

Hijacket Elma Original merupakan seri hijacket gaya modern yang tak lekang oleh waktu, hijacket elma dilengkapi dengan bentuk feminin, saku yang praktis dan pas serbaguna dan dengan tombol pearlescent vintage-chic. Sangat cocok untuk hijaber yang suka tampil formal dan berstyle jas. Yuk miliki sekarang juga ! 